# xxx_face_xxx
----
# Introduction
----
This project using RetinaFace-MobileNet0.25 to detect face, Resnet100 and Norm 2 to recognize face and Kalman filter to track face on surveillance camera.
# Requirement
----
* Install opencv.
* Install insightface
# Result
----
#### FPS of algorithm:
![FPS of algorithm](picture/fps.png)

# Usage
----
## 1. rcnn error
If rcnn error, using the following syntax:
```
cd rcnn/cython
sudo python3 setup.py install
cp build/lib.linux-x86_64-3.6/*.so . 
```
## 2. Generate training set and test set:
Using [this script](https://bitbucket.org/minhtan97/xxx_face_xxx/src/master/generate_db.py) to generate training set and test set as following command:
```
bash generate_db.sh
```
It will create train.txt, test.txt and combine.txt in folder db/db1
## 3. Feature extraction:
Using [this script](https://bitbucket.org/minhtan97/xxx_face_xxx/src/master/extract_features.py) to extract feature of images. The systax as following:
#### To extract feature of training set:
```
bash extract_features.sh
```
## 4. Find threshold:
Using [this script](https://bitbucket.org/minhtan97/xxx_face_xxx/src/master/find_thresh.py) to find threshold of known and unknown face, you could modify num_class in code. The syntax as following:
```
python3 find_thresh.py
```
## 5. Testing:
Using [this script](https://bitbucket.org/minhtan97/xxx_face_xxx/src/master/test.py) to test model. The syntax as following:
```
bash test.sh <PATH_OF_IMAGE>
```
## 6. Testing video or camera:
Using [this script](https://bitbucket.org/minhtan97/xxx_face_xxx/src/master/camera_test.py) to test with video or camera. The syntax as following:
```
bash camera_test.sh <PATH_OF_VIDEO_OR_IP_RTSP>
```
It will record video after applying algorithm.
# References
----
## Repositories
* [Insight Face](https://github.com/deepinsight/insightface)
* [RetinaFace MobileNet0.25](https://github.com/deepinsight/insightface/issues/669)
* [Smaller Is Better: Lightweight Face Detection For Smartphones](https://medium.com/syncedreview/smaller-is-better-lightweight-face-detection-for-smartphones-bcd27a1a1a82)
* [Face track detect Extract](https://github.com/Linzaer/Face-Track-Detect-Extract)
* [Experimenting with sort](https://github.com/ZidanMusk/experimenting-with-sort)
* [Deep SORT](https://nanonets.com/blog/object-tracking-deepsort/#deep-sort)
* [Kalman filter](https://www.codeproject.com/articles/865935/object-tracking-kalman-filter-with-ease)
* [A Light and Fast Face Detector for Edge Devices](https://github.com/YonghaoHe/A-Light-and-Fast-Face-Detector-for-Edge-Devices)
* [Hungary algorithm](http://www.math.harvard.edu/archive/20_spring_05/handouts/assignment_overheads.pdf)
* [Mahalonobis Distance](https://www.machinelearningplus.com/statistics/mahalanobis-distance/)
* [Real-time Human Detection in Computer Vision — Part 1](https://medium.com/@madhawavidanapathirana/https-medium-com-madhawavidanapathirana-real-time-human-detection-in-computer-vision-part-1-2acb851f4e55)
* [Real-time Human Detection in Computer Vision — Part 2](https://medium.com/@madhawavidanapathirana/real-time-human-detection-in-computer-vision-part-2-c7eda27115c6)
* [Data visualization with T-sne](https://viblo.asia/p/data-visualization-voi-thuat-toan-t-sne-su-dung-tensorflow-projector-924lJAAzZPM)
* [Accelerating TSNE with GPUs: From hours to seconds](https://medium.com/rapids-ai/tsne-with-gpus-hours-to-seconds-9d9c17c941db)
## Documentations
* [RetinaFace: Single-stage Dense Face Localisation in the Wild](https://arxiv.org/pdf/1905.00641.pdf)
* [The Menpo Benchmark for Multi-pose 2D and 3D Facial Landmark Localisation and Tracking](https://www.researchgate.net/publication/329278466_The_Menpo_Benchmark_for_Multi-pose_2D_and_3D_Facial_Landmark_Localisation_and_Tracking)
* [Stacked Dense U-Nets with DualTransformers for Robust Face Alignment](https://ibug.doc.ic.ac.uk/media/uploads/documents/bmvc2018_face_alignment.pdf)
* [ArcFace: Additive Angular Margin Loss for Deep Face Recognition](https://arxiv.org/pdf/1801.07698.pdf)
* [Deep Face Recognition: A Survey](https://arxiv.org/pdf/1804.06655.pdf)
* [Simple Online and Realtime Tracking](https://arxiv.org/pdf/1602.00763.pdf)
* [Simple Online and Realtime Tracking with a Deep Association Metric](https://arxiv.org/pdf/1703.07402.pdf)
* [LFFD: A Light and Fast Face Detector for Edge Devices](https://arxiv.org/pdf/1904.10633.pdf)

