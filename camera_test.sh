#!/bin/sh
#export PYTHONPATH=models/insightface/deploy/
#python3 camera_test.py --video-path $1 --image-size 112,112 --model models/insightface/models/model-r100-ii/model,0
sed -i "s|PATH|'$1'|g" camera_test.py
python3 camera_test.py
sed -i "s|'$1'|PATH|g" camera_test.py
