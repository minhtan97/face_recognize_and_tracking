from face import Face
import cv2

# face_rcn = Face()
face_rcn = Face(model_path='./models/R25/mnet.25')
# img = cv2.imread('test/images/46.png')
# result = face_rcn.face_recognition(img, True)
# cv2.imshow("result", result)
# cv2.waitKey(0)


cap = cv2.VideoCapture(PATH)
i = 0
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
 
# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
out = cv2.VideoWriter('out_rcn.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 20, (frame_width,frame_height))
while(True):
    ret, img = cap.read()
    if ret == True:
        result = face_rcn.face_tracking(img, show_id=True)
        i+=1
        # result = face_rcn.face_recognition(img, scale=[600,800], draw_landmark=True)
        out.write(result)
        cv2.imshow('Tracking', result)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

mean_fps_detect, mean_fps_emb, mean_fps_recognize, mean_fps_tracking = face_rcn.get_mean_fps(i)
print("FPS: ", mean_fps_tracking)
face_rcn.get_tracking_inf(i)
# face_rcn.get_recognition_inf(i)
cap.release()
out.release()
cv2.destroyAllWindows()