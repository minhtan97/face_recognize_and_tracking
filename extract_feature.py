import cv2
import sys
import numpy as np
import datetime
import os
import glob
from retinaface import RetinaFace
import sys
from skimage import transform as trans
import face_model
import argparse

count = 1

gpuid = 0
detector = RetinaFace('./models/R50/R50', 0, gpuid, 'net3')

parser = argparse.ArgumentParser(description='face model test')
# general
parser.add_argument('--image-size', default='112,112', help='')
parser.add_argument('--model', default='', help='path to load model.')
parser.add_argument('--ga-model', default='', help='path to load model.')
parser.add_argument('--gpu', default=0, type=int, help='gpu id')
parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
args = parser.parse_args()

print(args.model)
model = face_model.FaceModel(args)


def alignment(cv_img, dst, dst_w, dst_h):
    if dst_w == 112 and dst_h == 112:
        src = np.array([
            [38.2946, 51.6963],
            [73.5318, 51.5014],
            [56.0252, 71.7366],
            [41.5493, 92.3655],
            [70.7299, 92.2041] ], dtype=np.float32)
    tform = trans.SimilarityTransform()
    tform.estimate(dst, src)
    M = tform.params[0:2,:]
    face_img = cv2.warpAffine(cv_img,M,(dst_w,dst_h), borderValue = 0.0)
    return face_img

def save_feature(save_path, feature):    
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    print("[+]Save extracted feature to file : ", save_path)
    np.save(save_path, feature)

def save_image(save_path, image):    
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    print("[+]Save roi image : ", save_path)
    cv2.imwrite(save_path, image)

def extract(img_path, save_path, scales, thresh):
    img = cv2.imread(img_path)
    im_shape = img.shape
    target_size = scales[0]
    max_size = scales[1]
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])
    #im_scale = 1.0
    #if im_size_min>target_size or im_size_max>max_size:
    im_scale = float(target_size) / float(im_size_min)
    # prevent bigger axis from being more than max_size:
    if np.round(im_scale * im_size_max) > max_size:
        im_scale = float(max_size) / float(im_size_max)


    scales = [im_scale]
    flip = False

    for c in range(count):
        faces, landmarks = detector.detect(img, thresh, scales=scales, do_flip=flip)

    if faces is not None:
        print('find', faces.shape[0], 'faces')
        if (faces.shape[0] > 1):
            max_area = []
            for i in range(faces.shape[0]):
                box = faces[i].astype(np.int)
                height = box[3] - box[1]
                width = box[2] - box[0]
                max_area.append(height*width)
            
            position = np.argsort(max_area)[-1]
            print(max_area)
            print(position)
            box = faces[position].astype(np.int)
            # print(box.shape)
            roi = img[box[1]:box[3], box[0]:box[2]]
            
            height = box[3] - box[1]
            width = box[2] - box[0]
            ratio = [112/width, 112/height]
            # print(ratio)
            roi = cv2.resize(roi, (112,112))

            if landmarks is not None:
                if landmarks.shape[0] != 0:
                    landmark = landmarks[position].astype(np.int)
                    for l in range(landmark.shape[0]):
                        landmark[l][0] = (landmark[l][0] - box[0]) * ratio[0]
                        landmark[l][1] = (landmark[l][1] - box[1]) * ratio[1]

                    p1 = landmark[0]
                    p2 = landmark[1]
                    p3 = landmark[2]
                    p4 = landmark[3]
                    p5 = landmark[4]

                    dst = np.array([p1,p2,p3,p4,p5],dtype=np.float32)
                    face_112x112 = alignment(roi, dst, 112, 112)
                    face_transpose = np.transpose(face_112x112, (2,0,1))
                    emb = model.get_feature(face_transpose)
                    save_feature(save_path, emb)
                    save_im_path = 'result/roi/' + img_path.split('/')[-2] + '/' + img_path.split('/')[-1]
                    save_image(save_im_path, roi)
        else:
            for i in range(faces.shape[0]):
            
                box = faces[i].astype(np.int)
                roi = img[box[1]:box[3], box[0]:box[2]]
                # print(roi.shape)
                height = box[3] - box[1]
                width = box[2] - box[0]
                ratio = [112/width, 112/height]
                # print(ratio)
                roi = cv2.resize(roi, (112,112))

                if landmarks is not None:
                    if landmarks.shape[0] != 0:
                        landmark = landmarks[i].astype(np.int)
                        for l in range(landmark.shape[0]):
                            landmark[l][0] = (landmark[l][0] - box[0]) * ratio[0]
                            landmark[l][1] = (landmark[l][1] - box[1]) * ratio[1]
                        p1 = landmark[0]
                        p2 = landmark[1]
                        p3 = landmark[2]
                        p4 = landmark[3]
                        p5 = landmark[4]

                        dst = np.array([p1,p2,p3,p4,p5],dtype=np.float32)
                        face_112x112 = alignment(roi, dst, 112, 112)
                        face_transpose = np.transpose(face_112x112, (2,0,1))
                        emb = model.get_feature(face_transpose)
                        save_feature(save_path, emb)
                        save_im_path = 'result/roi/' + img_path.split('/')[-2] + '/' + img_path.split('/')[-1]
                        save_image(save_im_path, roi)

def extract_features(src):
    thresh = 0.8
    with open(src, "r") as file:
        for i,line in enumerate(file):
            img_path = line[:-1]
            scales = [1024, 1980]
            print("[+] Read image  : ", img_path," id : ", i)
            if os.path.isfile(img_path) and img_path.find(".png") != -1:            
                save_path = img_path.replace("images", "features").replace(".png", ".npy")            
                extract(img_path, save_path, scales, thresh)
            elif os.path.isfile(img_path) and img_path.find(".jpg") != -1:            
                save_path = img_path.replace("images", "features").replace(".jpg", ".npy")            
                extract(img_path, save_path, scales, thresh)
                      

if __name__=="__main__":
    combine = 'db/db1/combine.txt'
    extract_features(combine)
