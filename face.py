import cv2
import numpy as np
import os
import time
import copy
import sys
import argparse
from track import Tracker
from retinaface import RetinaFace
from skimage import transform as trans
from scipy.optimize import linear_sum_assignment
sys.path.append('models/insightface/deploy/')
import face_model


class Face():
    def __init__(self, model_path = './models/R50/R50', 
                    feature_path = 'features', 
                    tracker_path = 'tracking',
                    image_track_path = 'result/tracking',
                    gpuid = 0, 
                    dist_thresh = 0.25, 
                    max_frames_to_skip = 3,
                    max_trace_length = 5,
                    track_id_count = 1):

        self.remove_exist_file(tracker_path)
        self.remove_exist_file(image_track_path)
        self.detector = RetinaFace(model_path, 0, gpuid, 'net3')
        self.feature_path = feature_path
        self.tracker_path = tracker_path
        self.tracker = Tracker(dist_thresh, max_frames_to_skip, max_trace_length, track_id_count)
        self.features, self.labels = self.load_features(feature_path)
        self.fps_detect = 0
        self.fps_recognize = 0
        self.fps_emb = 0
        self.fps_tracking = 0
        self.load_model_detect = True
        self.load_model_recognize = True
        self.load_model_emb = True
        self.num_object = 0

        parser = argparse.ArgumentParser(description='face model test')
        parser.add_argument('--image-size', default='112,112', help='')
        parser.add_argument('--model', default='models/insightface/models/model-r100-ii/model,0', help='path to load model.')
        parser.add_argument('--ga-model', default='', help='path to load model.')
        parser.add_argument('--gpu', default=0, type=int, help='gpu id')
        parser.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
        parser.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
        parser.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')
        self.args = parser.parse_args()

        self.model = face_model.FaceModel(self.args)

    
    def remove_exist_file(self, path_src):
        files = [f for f in os.listdir(path_src) if os.path.isfile(os.path.join(path_src, f))]
        for f in files:
            os.remove(os.path.join(path_src, f))


    def alignment(self, img, dst, dst_w, dst_h):
        if dst_w == 112 and dst_h == 112:
            src = np.array([
                [38.2946, 51.6963],
                [73.5318, 51.5014],
                [56.0252, 71.7366],
                [41.5493, 92.3655],
                [70.7299, 92.2041] ], dtype=np.float32)
        tform = trans.SimilarityTransform()
        tform.estimate(dst, src)
        M = tform.params[0:2,:]
        face_img = cv2.warpAffine(img, M, (dst_w,dst_h), borderValue = 0.0)
        return face_img


    def face_distance(self, face_encodings, labels, face_to_compare, tolerance, num_vote):
        vote_arr = []
        origin_arr = []
        major_vote = []
        same = False
        if len(face_encodings) == 0:
            return np.empty((0))

        preds = np.linalg.norm(face_encodings - face_to_compare, axis=1)
        print(preds[0])
        sort_prob = np.argsort(preds)
        if preds[sort_prob[0]] > tolerance:
            return -1
        else:
            print(labels[sort_prob[0]], labels[sort_prob[1]], labels[sort_prob[2]], labels[sort_prob[3]], labels[sort_prob[4]])
            vote_arr.append(labels[sort_prob[0]])
            origin_arr.append(labels[sort_prob[0]])
            for i in range(num_vote - 1):
                origin_arr.append(labels[sort_prob[i+1]])
                for j in range(len(vote_arr)):
                    if(labels[sort_prob[i+1]] == vote_arr[j]):
                        same = True
                if(same == False):
                    vote_arr.append(labels[sort_prob[i+1]])
                else:
                    same = False
                    
            if(len(vote_arr) < num_vote and len(vote_arr) != 1 ):
                for i in range(len(vote_arr)):
                    major_vote.append(origin_arr.count(vote_arr[i]))
                sort_major_vote = np.argsort(major_vote)
                return vote_arr[sort_major_vote[-1]]
            else:
                return labels[sort_prob[0]]

    def compare_faces(self, known_face_encodings, labels, face_encoding_to_check, tolerance=1.16, num_vote = 5):
        label = self.face_distance(known_face_encodings, labels, face_encoding_to_check, tolerance, num_vote)
        if label == -1:
            return 'unknown'
        else:
            return label

    def load_features(self, data_dir):
        print("[+] Load data....")
        data = []
        labels = []
        directories = [d for d in os.listdir(data_dir) 
                    if os.path.isdir(os.path.join(data_dir, d))]
        for d in directories:
            label_dir = os.path.join(data_dir, d)
            file_names = [os.path.join(label_dir, f) 
                        for f in os.listdir(label_dir) if f.endswith(".npy")]
            for f in file_names:
                data.append(np.load(f))
                labels.append(d)

        print("[+] Load data finished")
        return data, labels

    def face_detect(self, img, scale, flip = False, thresh = 0.8):
        im_shape = img.shape
        target_size = scale[0]
        max_size = scale[1]
        im_size_min = np.min(im_shape[0:2])
        im_size_max = np.max(im_shape[0:2])
        im_scale = float(target_size) / float(im_size_min)
        if np.round(im_scale * im_size_max) > max_size:
            im_scale = float(max_size) / float(im_size_max)
        scales = [im_scale]
        if(self.load_model_detect == False):
            start_dect = time.time()
        faces, landmarks = self.detector.detect(img, thresh, scales=scales, do_flip=flip)
        # print("Detector FPS: ", 1/(time.time() - start))
        if(self.load_model_detect):
            self.load_model_detect = False
        else:
            self.fps_detect += 1/(time.time() - start_dect)
        return faces, landmarks


    def face_recognition(self, img, scale = [600, 800], draw_landmark = False):
        if (self.load_model_recognize == False):
            start_recog = time.time()
        faces, landmarks = self.face_detect(img, scale)
        result = copy.deepcopy(img)

        if faces is not None:
            print('find', faces.shape[0], 'faces')
            self.num_object += faces.shape[0]
            for i in range(faces.shape[0]):
                box = faces[i].astype(np.int)
                if (box[0] < 15 or box[1] < 15 or box[2] > img.shape[1] - 15 or box[3] > img.shape[0] - 15):
                    self.num_object = self.num_object - 1
                else:
                    roi = img[box[1]:box[3], box[0]:box[2]]

                    height = box[3] - box[1]
                    width = box[2] - box[0]
                    ratio = [112/width, 112/height]

                    roi = cv2.resize(roi, (112,112))

                    if landmarks is not None:
                        if landmarks.shape[0] != 0:
                            landmark = landmarks[i].astype(np.int)
                            for l in range(landmark.shape[0]):
                                if (draw_landmark):
                                    color = (0,0,255)
                                    if l==0 or l==3:
                                        color = (0,255,0)
                                    cv2.circle(result, (landmark[l][0], landmark[l][1]), 1, color, 2)
                                    
                                landmark[l][0] = (landmark[l][0] - box[0]) * ratio[0]
                                landmark[l][1] = (landmark[l][1] - box[1]) * ratio[1]
                                

                            dst = np.array([landmark[0], landmark[1] ,landmark[2], landmark[3], landmark[4]],dtype=np.float32)

                            face_112x112 = self.alignment(roi, dst, 112, 112)
                            face_transpose = np.transpose(face_112x112, (2,0,1))

                            if (self.load_model_emb == False):
                                start_emb = time.time()
                            emb = self.model.get_feature(face_transpose)
                            if(self.load_model_emb):
                                self.load_model_emb = False
                            else:
                                self.fps_emb += 1/(time.time() - start_emb)

                            label = self.compare_faces(self.features, self.labels, emb)

                            startX = box[0]
                            startY = box[1] - 15 if box[1] - 15 > 15 else box[1] + 15
                            cv2.putText(result, label, (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)

                            cv2.rectangle(result, (box[0], box[1]), (box[2], box[3]), color, 2)
        if(self.load_model_recognize):
            self.load_model_recognize = False
        else:
            self.fps_recognize += 1/(time.time() - start_recog)
        return result


    def face_tracking(self, img, scale = [600, 800], show_id = False):
        start_track = time.time()
        faces, landmarks = self.face_detect(img, scale)
        result = copy.deepcopy(img)

        det = []
        landmark_arr = []
        thumbs = []

        if faces is not None:
            print('find', faces.shape[0], 'faces')
            for i in range(faces.shape[0]):
                box = faces[i].astype(np.int)
                # print(img.shape)
                if (box[0] < 15 or box[1] < 15 or box[2] > img.shape[1] - 15 or box[3] > img.shape[0] - 15):
                    continue
                else:
                    det.append(box)
                    thumbs.append(img[box[1]:box[3], box[0]:box[2]])
                    if landmarks is not None:
                        if landmarks.shape[0] != 0:
                            landmark_arr.append(landmarks[i].astype(np.int))

        self.tracker.update(landmark_arr, det, thumbs, self.tracker_path, self.model, self.features, self.labels)
        self.fps_tracking += 1/(time.time() - start_track)

        for i in range(len(self.tracker.tracks)):
            cv2.rectangle(result, (self.tracker.tracks[i].det[0], self.tracker.tracks[i].det[1]), (self.tracker.tracks[i].det[2], self.tracker.tracks[i].det[3]), (255,0,0), 1)
            startX = int(self.tracker.tracks[i].det[0])
            startY = int(self.tracker.tracks[i].det[1] - 15) if self.tracker.tracks[i].det[1] - 15 > 15 else self.tracker.tracks[i].det[1] + 15
            if show_id:
                cv2.putText(result, str(self.tracker.tracks[i].track_id), (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
            else:
                cv2.putText(result, str(self.tracker.tracks[i].label), (startX, startY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
        
        return result

    
    def get_mean_fps(self, total):
        return self.fps_detect/total, self.fps_emb/total, self.fps_recognize/total, self.fps_tracking/total

    def get_detection_inf(self, total):
        print("Sum of frame: ", total)
        print("FPS:", self.fps_detect/total)
    
    def get_recognition_inf(self, total):
        print("Sum of frame: ", total)
        print("Sum of object: ", self.num_object)
        print("FPS:", self.fps_recognize/total)

    def get_tracking_inf(self, total):
        print("Sum of frame: ", total)
        print("FPS:", self.fps_tracking/total)