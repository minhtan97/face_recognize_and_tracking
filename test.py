from face import Face
import cv2
import os

# face_rcn = Face()
face_rcn = Face(model_path='./models/R25/mnet.25')

file_names = [os.path.join(PATH, f) 
            for f in os.listdir(PATH) if f.endswith(".jpg")]

for f in file_names:
    img = cv2.imread(f)
    result = face_rcn.face_recognition(img, scale=[1080,1920], draw_landmark=True)
    name = "result_p/MobileNet_ResNet100_1920x1080/" + f.split('/')[-1]
    cv2.imwrite(name, result)


mean_fps_detect, mean_fps_emb, mean_fps_recognize = face_rcn.get_mean_fps(len(file_names))
print("FPS detect", mean_fps_detect)
print("FPS embedding", mean_fps_emb)
print("FPS recognize", mean_fps_recognize)