import numpy as np
import os
import cv2
from scipy.optimize import linear_sum_assignment
from skimage import transform as trans
from lib.kalman_tracker import KalmanBoxTracker
from numba import jit
import copy

class Track(object):
    def __init__(self, det, trackIdCount):
        self.track_id = trackIdCount  # identification of each track object
        self.label = ""
        self.KF = KalmanBoxTracker(det)
        self.skipped_frames = 0  # number of frames skipped undetected
        self.trace = []  # trace path
        self.det = det
        self.store_feature = False

    def predict(self):
        return self.KF.predict()

    def update(self, arr):
        return self.KF.update(arr)


class Tracker(object):
    def __init__(self, dist_thresh, max_frames_to_skip, max_trace_length, trackIdCount):
        self.dist_thresh = dist_thresh
        self.max_frames_to_skip = max_frames_to_skip
        self.max_trace_length = max_trace_length
        self.tracks = []
        self.trackIdCount = trackIdCount


    def judge_front_face(self, facial_landmarks, thresh_dist_min = 0.7, thresh_dist_max = 1.7, thresh_high_std = 0.5, thresh_width_std = 0.3):
        if (facial_landmarks[2][0] <= facial_landmarks[0][0] or facial_landmarks[2][0] >= facial_landmarks[1][0]
            or facial_landmarks[2][0] <= facial_landmarks[3][0] or facial_landmarks[2][0] >= facial_landmarks[4][0]):
            return False
        # print(facial_landmarks)
        
        wide_dist = np.linalg.norm(facial_landmarks[0] - facial_landmarks[1])
        high_dist = np.linalg.norm(facial_landmarks[0] - facial_landmarks[3])
        dist_rate = high_dist / wide_dist

        # cal std
        vec_A = facial_landmarks[0] - facial_landmarks[2]
        vec_C = facial_landmarks[3] - facial_landmarks[2]
        dist_A = np.linalg.norm(vec_A)
        dist_C = np.linalg.norm(vec_C)

        # cal rate
        high_rate = dist_A / dist_C
        high_ratio_std = np.fabs(high_rate - 1.1)  # smaller is better
        print(dist_rate, high_ratio_std)
        if(dist_rate < thresh_dist_min or dist_rate > thresh_dist_max or high_ratio_std > thresh_high_std):
            return False
        return True


    def iou(self, bb_test, bb_gt):
        """
        Computes IUO between two bboxes in the form [x1,y1,x2,y2]
        """
        xx1 = np.maximum(bb_test[0], bb_gt[0])
        yy1 = np.maximum(bb_test[1], bb_gt[1])
        xx2 = np.minimum(bb_test[2], bb_gt[2])
        yy2 = np.minimum(bb_test[3], bb_gt[3])
        w = np.maximum(0., xx2 - xx1)
        h = np.maximum(0., yy2 - yy1)
        wh = w * h
        o = wh / ((bb_test[2] - bb_test[0]) * (bb_test[3] - bb_test[1])
                + (bb_gt[2] - bb_gt[0]) * (bb_gt[3] - bb_gt[1]) - wh)
        return o


    def alignment(self, img, dst, dst_w, dst_h):
        if dst_w == 112 and dst_h == 112:
            src = np.array([
                [38.2946, 51.6963],
                [73.5318, 51.5014],
                [56.0252, 71.7366],
                [41.5493, 92.3655],
                [70.7299, 92.2041] ], dtype=np.float32)
        tform = trans.SimilarityTransform()
        tform.estimate(dst, src)
        M = tform.params[0:2,:]
        face_img = cv2.warpAffine(img, M, (dst_w,dst_h), borderValue = 0.0)
        return face_img


    def save_feature(self, save_path, feature):    
        os.makedirs(os.path.dirname(save_path), exist_ok=True)

        print("[+]Save extracted feature to file : ", save_path)
        np.save(save_path, feature)


    def save_image(self, save_path, image):    
        os.makedirs(os.path.dirname(save_path), exist_ok=True)

        print("[+]Save roi image : ", save_path)
        cv2.imwrite(save_path, image)


    def load_features(self, src):
        print("[+] Load data....")
        data = []
        label = []
        files = [f for f in os.listdir(src) if os.path.isfile(os.path.join(src, f))]
        for i, f in enumerate(files):
            # print(f)
            data.append(np.load(os.path.join(src, f)))
            label.append(f.split(".")[0])
        print("[+] Load data finished")
        return np.array(data), np.array(label)


    def compare_feature_memory(self, thumb, src, tolerance):
        features, labels = self.load_features(src)

        if len(features) == 0:
            return -1
        preds = np.linalg.norm(features - thumb, axis=1)
        sort_prob = np.argsort(preds)
        # print(preds)
        print('------------->', preds[sort_prob[0]])
        if preds[sort_prob[0]] > tolerance:
            return -1
        else:
            return int(labels[sort_prob[0]])


    def face_distance(self, face_encodings, labels, face_to_compare, tolerance, num_vote):
        vote_arr = []
        origin_arr = []
        major_vote = []
        same = False
        if len(face_encodings) == 0:
            return np.empty((0))

        preds = np.linalg.norm(face_encodings - face_to_compare, axis=1)
        sort_prob = np.argsort(preds)
        if preds[sort_prob[0]] > tolerance:
            return -1
        else:
            print(labels[sort_prob[0]], labels[sort_prob[1]], labels[sort_prob[2]], labels[sort_prob[3]], labels[sort_prob[4]])
            vote_arr.append(labels[sort_prob[0]])
            origin_arr.append(labels[sort_prob[0]])
            for i in range(num_vote - 1):
                origin_arr.append(labels[sort_prob[i+1]])
                for j in range(len(vote_arr)):
                    if(labels[sort_prob[i+1]] == vote_arr[j]):
                        same = True
                if(same == False):
                    vote_arr.append(labels[sort_prob[i+1]])
                else:
                    same = False
                    
            if(len(vote_arr) < num_vote and len(vote_arr) != 1 ):
                for i in range(len(vote_arr)):
                    major_vote.append(origin_arr.count(vote_arr[i]))
                sort_major_vote = np.argsort(major_vote)
                return vote_arr[sort_major_vote[-1]]
            else:
                return labels[sort_prob[0]]

    def compare_faces(self, known_face_encodings, labels, face_encoding_to_check, tolerance=1.16, num_vote = 5):
        label = self.face_distance(known_face_encodings, labels, face_encoding_to_check, tolerance, num_vote)
        if label == -1:
            return 'unknown'
        else:
            return label

    def save_inf_object(self, roi, landmark, save_path, emb, draw_landmark = False):
        print('-----------> save feature')
        self.save_feature(save_path, emb)
        save_im_path = 'result/tracking/' + save_path.split('/')[-1].replace(".npy", ".jpg")
        if draw_landmark:
            img2 = copy.deepcopy(roi)
            for l in range(landmark.shape[0]):
                color = (0,0,255)
                if l==0 or l==3:
                    color = (0,255,0)
                cv2.circle(img2, (landmark[l][0], landmark[l][1]), 1, color, 2)
            self.save_image(save_im_path, img2)
        else:
            self.save_image(save_im_path, roi)


    def extract(self, roi, landmark, model):
        dst = np.array([landmark[0], landmark[1], landmark[2], landmark[3], landmark[4]],dtype=np.float32)
        face_112x112 = self.alignment(roi, dst, 112, 112)
        face_transpose = np.transpose(face_112x112, (2,0,1))
        emb = model.get_feature(face_transpose)
        return emb



    def update(self, landmark_arr, det, thumb, src, model, features, labels, tolerance = 1.17):
        """Update tracks vector using following steps:
            - Create tracks if no tracks vector found
            - Calculate cost using intersection over union (IoU) between predicted vs detected bounding box
            - Using Hungarian Algorithm assign the correct detected measurements to predicted tracks
            - Identify tracks with no assignment, if any
            - If tracks are not detected for long time, remove them
            - Start new tracks for un_assigned detects
            - Update KalmanFilter state, lastResults and tracks trace
        Args:
            detections: Information (5 point landmark, bounding box, face) of object to be tracked, tracking path, features and labels in database.
        Return:
            None
        """

        # Create tracks if no tracks vector found
        if (len(self.tracks) == 0):
            for i in range(len(det)):
                if (self.judge_front_face(landmark_arr[i])):
                    landmark_arr_tmp = landmark_arr[i].astype(np.int)
                    height = det[i][3] - det[i][1]
                    width = det[i][2] - det[i][0]
                    ratio = [112/width, 112/height]
                    roi = cv2.resize(thumb[i], (112,112))
                    
                    for l in range(landmark_arr[i].shape[0]):
                        landmark_arr_tmp[l][0] = (landmark_arr_tmp[l][0] - det[i][0]) * ratio[0]
                        landmark_arr_tmp[l][1] = (landmark_arr_tmp[l][1] - det[i][1]) * ratio[1]

                    face_emb = self.extract(roi, landmark_arr_tmp, model)

                    _id = self.compare_feature_memory(face_emb, src, tolerance)
                    if _id != -1:
                        track = Track(det[i], _id)
                        track.label = self.compare_faces(features, labels, face_emb)
                        self.tracks.append(track)
                    else:
                        track = Track(det[i], self.trackIdCount)
                        if (self.judge_front_face(landmark_arr[i])):
                            save_path = os.path.join(src, str(track.track_id) + ".npy")
                            self.save_inf_object(roi, landmark_arr_tmp, save_path, face_emb, draw_landmark = True)
                            track.label = self.compare_faces(features, labels, np.load(save_path))
                            self.tracks.append(track)
                            self.tracks[-1].store_feature = True
                        else:
                            self.tracks.append(track)
                        self.trackIdCount += 1
        else:
            if len(det) == 0:
                del_tracks = []
                for i, trks in enumerate(self.tracks):
                    trks.update([])
                    trks.skipped_frames += 1
                    if (trks.skipped_frames > self.max_frames_to_skip):
                        del_tracks.append(i)
                if len(del_tracks) > 0:  # only when skipped frame exceeds max
                    for id in del_tracks:
                        if id < len(self.tracks):
                            del self.tracks[id]
            else:
                # Calculate cost using sum of square distance between
                # predicted vs detected centroids
                N = len(self.tracks)
                M = len(det)

                cost = np.zeros((len(self.tracks), len(det)), dtype=np.float32)   # Cost matrix
                for i, trks in enumerate(self.tracks):
                    for j, d in enumerate(det):
                        cost[i, j] = self.iou(trks.predict(), d)

                # Using Hungarian Algorithm assign the correct detected measurements
                # to predicted tracks
                assignment = []
                for _ in range(N):
                    assignment.append(-1)
                row_ind, col_ind = linear_sum_assignment(-cost)
                for i in range(len(row_ind)):
                    assignment[row_ind[i]] = col_ind[i]

                # Identify tracks with no assignment, if any
                un_assigned_tracks = []
                for i in range(len(assignment)):
                    if (assignment[i] != -1):
                        # check for cost distance threshold.
                        # If cost is very high then un_assign (delete) the track
                        if (cost[i][assignment[i]] < self.dist_thresh):
                            assignment[i] = -1
                            un_assigned_tracks.append(i)
                            self.tracks[i].skipped_frames += 1
                        pass
                    else:
                        self.tracks[i].skipped_frames += 1
                                
                # If tracks are not detected for long time, remove them
                del_tracks = []
                for i in range(len(self.tracks)):
                    if (self.tracks[i].skipped_frames > self.max_frames_to_skip):
                        del_tracks.append(i)
                if len(del_tracks) > 0:  # only when skipped frame exceeds max
                    for id in del_tracks:
                        if id < len(self.tracks):
                            del self.tracks[id]
                            del assignment[id]

                # Now look for un_assigned detects
                un_assigned_detects = []
                for i in range(len(det)):
                    if i not in assignment:
                        un_assigned_detects.append(i)

                # Start new tracks
                if(len(un_assigned_detects) != 0):
                    for i in range(len(un_assigned_detects)):
                        if (self.judge_front_face(landmark_arr[un_assigned_detects[i]])):
                            landmark_arr_tmp = landmark_arr[un_assigned_detects[i]].astype(np.int)
                            height = det[un_assigned_detects[i]][3] - det[un_assigned_detects[i]][1]
                            width = det[un_assigned_detects[i]][2] - det[un_assigned_detects[i]][0]
                            ratio = [112/width, 112/height]
                            roi = cv2.resize(thumb[un_assigned_detects[i]], (112,112))
                            
                            for l in range(landmark_arr[un_assigned_detects[i]].shape[0]):
                                landmark_arr_tmp[l][0] = (landmark_arr_tmp[l][0] - det[un_assigned_detects[i]][0]) * ratio[0]
                                landmark_arr_tmp[l][1] = (landmark_arr_tmp[l][1] - det[un_assigned_detects[i]][1]) * ratio[1]

                            face_emb = self.extract(roi, landmark_arr_tmp, model)
                           
                            _id = self.compare_feature_memory(face_emb, src, tolerance)
                            exist_id = []
                            for j in range(len(self.tracks)):
                                exist_id.append(self.tracks[j].track_id)
                            if _id in exist_id:
                                _id = -1
                            print(_id)
                            if  _id != -1:
                                track = Track(det[un_assigned_detects[i]], _id)
                                track.label = self.compare_faces(features, labels, face_emb)
                                self.tracks.append(track)
                            else:
                                track = Track(det[un_assigned_detects[i]], self.trackIdCount)
                                self.tracks.append(track)
                                if (self.judge_front_face(landmark_arr[un_assigned_detects[i]])):
                                    print(landmark_arr[un_assigned_detects[i]])
                                    save_path = os.path.join(src, str(self.tracks[-1].track_id) + ".npy")
                                    self.save_inf_object(roi, landmark_arr_tmp, save_path, face_emb, draw_landmark = True)
                                    self.tracks[-1].label = self.compare_faces(features, labels, np.load(save_path))
                                    self.tracks[-1].store_feature = True
                                self.trackIdCount += 1
                    
                # Update KalmanFilter state, lastResults and tracks trace
                for i in range(len(assignment)):

                    if(assignment[i] != -1):
                        self.tracks[i].skipped_frames = 0
                        self.tracks[i].update(det[assignment[i]])
                        self.tracks[i].det = det[assignment[i]]
                    else:
                        self.tracks[i].update([])
